﻿using System;
using System.Collections.Generic;

namespace _31857086_TreinoPerceptron
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] m_InputValues = new double[8] { 0.3, 0.7, -0.6, 0.3, -0.1, -0.8, 0.1, -0.45 };
            double[] m_WeightValues = new double[2] { 0.8, -0.5 };
            int[] m_DesiredOutputs = new int[4] { 1, 0, 0, 1 };
            double m_LearningRate = 0.5;

            Perceptron m_Perceptron = new Perceptron(m_InputValues, m_WeightValues, m_DesiredOutputs, m_LearningRate);

            Console.WriteLine("Valores desejados dos pesos ajustados: 1,05 e 0,025.");
            Console.WriteLine("Valores dos pesos ajustados: ");
            foreach (double weight in m_Perceptron.BalancedWeights)
            {
                Console.WriteLine(weight);
            }

            Console.ReadKey();
        }
    }

    class Perceptron
    {
        double[] m_Inputs;
        double[] m_Weights;
        int[] m_DesiredOutputs;
        double m_LearningRate;

        List<double> m_BalancedWeights;
        public List<double> BalancedWeights { get => m_BalancedWeights; }

        double m_ResultSumForActivation;

        int m_CurrentResult;

        public Perceptron(double[] inputs, double[] weights, int[] desiredOutputs, double learningRate)
        {
            m_Inputs = new double[inputs.Length];
            m_Weights = new double[weights.Length];
            m_DesiredOutputs = new int[desiredOutputs.Length];
            m_LearningRate = learningRate;
            m_BalancedWeights = new List<double>();

            for (int i = 0; i < inputs.Length; ++i)
            {
                m_Inputs[i] = inputs[i];
            }

            for (int i = 0; i < weights.Length; ++i)
            {
                m_Weights[i] = weights[i];
            }

            for (int i = 0; i < desiredOutputs.Length; ++i)
            {
                m_DesiredOutputs[i] = desiredOutputs[i];
            }

            CalcNeuron();
        }

        void CalcNeuron()
        {
            List<double> currentInputs = new List<double>();
            Func<int, int, bool> CheckClassification = (x, y) => x == y ? true : false;
            Func<int, int, int> CalcError = (a, b) => b - a;

            for (int i = 0, j = 0; i < m_Inputs.Length; i += 2, ++j) //j vai controlar o valor atual do vetor m_DesiredOutput
            {
                currentInputs.Clear();
                currentInputs.Add(m_Inputs[i]);
                currentInputs.Add(m_Inputs[i + 1]);

                m_ResultSumForActivation = SumInputs(m_Inputs[i], m_Inputs[i + 1]);
                m_CurrentResult = ActivationFunction(m_ResultSumForActivation);

                if (!CheckClassification(m_CurrentResult, m_DesiredOutputs[j]))
                {
                    int error = CalcError(m_CurrentResult, m_DesiredOutputs[j]);
                    BalanceWeights(error, currentInputs);
                }
            }

            foreach (double weight in m_Weights)
            {
                m_BalancedWeights.Add(weight);
            }
        }

        void BalanceWeights(int error, List<double> inputs)
        {
            Func<double, double, int, double, double> Balance = (a, b, c, d) => a + (b * c * d);

            for (int i = 0; i < m_Weights.Length; ++i)
            {
                m_Weights[i] = Balance(m_Weights[i], m_LearningRate, error, inputs[i]);
            }
        }

        double SumInputs(double value1, double value2)
        {
            double sum = 0.0;

            sum += (value1 * m_Weights[0]) + (value2 * m_Weights[1]);

            return sum;
        }

        Func<double, int> ActivationFunction = x => x >= 0 ? 1 : 0;
    }
}