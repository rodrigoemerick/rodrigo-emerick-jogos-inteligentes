﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace Perceptron
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberFormatInfo number = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            number.NumberDecimalSeparator = ".";

            int m_NumberOfInputs;
            float[] m_InputValues;
            float[] m_WeightValues;
            float m_Bias;
            
            Console.WriteLine("Forneça a quantidade de inputs:");
            m_NumberOfInputs = int.Parse(Console.ReadLine());
            m_InputValues = new float[m_NumberOfInputs];
            m_WeightValues = new float[m_NumberOfInputs];

            Console.WriteLine("Forneça os valores dos inputs separados por um espaço:");
            string[] inputs = Console.ReadLine().Split();

            Console.WriteLine("Forneça os valores dos pesos separados por um espaço:");
            string[] weights = Console.ReadLine().Split();

            for (int i = 0; i < m_NumberOfInputs; ++i)
            {
                m_InputValues[i] = float.Parse(inputs[i], number);
                m_WeightValues[i] = float.Parse(weights[i], number);
            }

            Console.WriteLine("Forneça o valor do Bias/Threshold:");
            m_Bias = float.Parse(Console.ReadLine(), number);

            Perceptron m_Perceptron = new Perceptron(m_NumberOfInputs, m_InputValues, m_WeightValues, m_Bias);
            Console.WriteLine($"Output for this perceptron is: {m_Perceptron.Output}");
        }
    }

    class Perceptron
    {
        float[] m_Inputs;
        float[] m_Weights;
        float m_Bias;

        float m_Output;
        public float Output { get => m_Output; }

        float m_ResultSumForActivation;

        public Perceptron(int numberOfInputs, float[] inputs, float[] weights, float bias)
        {
            m_Inputs = new float[numberOfInputs];
            m_Weights = new float[numberOfInputs];

            for (int i = 0; i < numberOfInputs; ++i)
            {
                m_Inputs[i] = inputs[i];
                m_Weights[i] = weights[i]; 
            }

            m_Bias = bias;

            CalcNeuron();
        }

        void CalcNeuron()
        {
            m_ResultSumForActivation = SumInputs();
            m_Output = SigmoidActivationFunction(m_ResultSumForActivation);
        }

        float SumInputs()
        {
            float sum = 0.0f;

            sum -= m_Bias;

            for (int i = 0; i < m_Inputs.Length; ++i)
            {
                sum += m_Inputs[i] * m_Weights[i];
            }

            return sum;
        }

        Func<float, float> SigmoidActivationFunction = x => 1 / (1 + MathF.Exp(-x));
    }
}