﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    private int[,] m_Map;
    private GeneticAlgorithm m_GA;
    private List<int> m_FittestDirections;
    public List<GameObject> m_PathTiles;
    private float m_TimePassed;

    [Header("Map properties")]
    [SerializeField]
    private GameObject[] m_Tiles;
    [SerializeField]
    private float[] m_CamOffsets;
    [SerializeField]
    private Vector2 m_StartPosition;
    [SerializeField]
    private Vector2 m_EndPosition;

    [Header("GA properties")]
    [SerializeField]
    private double m_CrossoverRate;
    [SerializeField]
    private double m_MutationRate;
    [SerializeField]
    private int m_PopulationSize;
    [SerializeField]
    private int m_ChromosomeLength;
    [SerializeField]
    private int m_GeneLength;

    void Start()
    {
        m_Map = new int[,]
        {
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1},
            {3, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1},
            {1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1},
            {1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1},
            {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1},
            {1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2},
            {1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };

        CreateMapTiles();
        m_FittestDirections = new List<int>();
        m_PathTiles = new List<GameObject>();
        m_GA = GameObject.Find("GA").GetComponent<GeneticAlgorithm>();
        m_GA.InitializeGeneticAlgorithm(m_CrossoverRate, m_MutationRate, m_PopulationSize, m_ChromosomeLength, m_GeneLength);
        Debug.Log("Starting on position: " + m_StartPosition.x + ", " + m_StartPosition.y + " and trying to find path to position: " + m_EndPosition.x + ", " + m_EndPosition.y + ".");
    }
    
    void FixedUpdate()
    {
        if (m_GA.IsRunning)
        {
            m_GA.Epoch();
            m_TimePassed = Time.realtimeSinceStartup;
        }
        ShowPathOfFittestChromosome();
    }

    private void CreateMapTiles()
    {
        GameObject go;
        Vector3 position;
        int index = -1;

        for (int i = 0; i < m_Map.GetLength(0); ++i)
        {
            for (int j = 0; j < m_Map.GetLength(1); ++j)
            {
                switch (m_Map[i, j])
                {
                    case 0:
                        index = 0;
                        break;
                    case 1:
                        index = 1;
                        break;
                    case 2:
                        index = 2;
                        break;
                    case 3:
                        index = 3;
                        break;
                }
                go = Instantiate(m_Tiles[index]);
                position = new Vector3(j - m_CamOffsets[0], -i + m_CamOffsets[1], 0.0f);
                go.transform.position = position;
            }
        }
    }

    private Vector2 MoveToNextTile(Vector2 position, int direction)
    {
        switch (direction)
        {
            case 0:
                if (position.y - 1 < 0 || m_Map[(int)(position.y - 1), (int)position.x] == 1)
                    break;
                else
                    position.y -= 1;
                break;
            case 1:
                if (position.y + 1 >= m_Map.GetLength(0) || m_Map[(int)(position.y + 1), (int)position.x] == 1)
                    break;
                else
                    position.y += 1;
                break;
            case 2:
                if (position.x + 1 >= m_Map.GetLength(1) || m_Map[(int)(position.y), (int)position.x + 1] == 1)
                    break;
                else
                    position.x += 1;
                break;
            case 3:
                if (position.x - 1 < 0 || m_Map[(int)(position.y), (int)position.x - 1] == 1)
                    break;
                else
                    position.x -= 1;
                break;
        }

        return position;
    }

    public double TestRoute(int[] directions)
    {
        Vector2 position = m_StartPosition;
        int nextDirection;
        double deltaX, deltaY;
        double result;

        for (int directionIndex = 0; directionIndex < directions.Length; ++directionIndex)
        {
            nextDirection = directions[directionIndex];
            position = MoveToNextTile(position, nextDirection);
        }

        deltaX = Mathf.Abs(position.x - m_EndPosition.x);
        deltaY = Mathf.Abs(position.y - m_EndPosition.y);

        result = 1.0f / (deltaX + deltaY + 1.0f);

        if (result >= 1.0f)
        {
            Debug.LogWarning("Found exit on position: " + position.x + ", " + position.y + ". " + "Number of generations is: " + m_GA.Generation + ". " + "Time: " + m_TimePassed + " seconds.");
        }

        return result;
    }

    private void ClearPath()
    {
        foreach (GameObject tile in m_PathTiles)
        {
            Destroy(tile);
        }
        m_PathTiles.Clear();
    }

    private void ShowPathOfFittestChromosome()
    {
        ClearPath();

        Genome fittestGenome = m_GA.Genomes[m_GA.FittestGenome];
        int[] fittestDirections = m_GA.Decode(fittestGenome.Bits);
        Vector2 position = m_StartPosition;

        GameObject go;
        Vector3 tilePosition;

        foreach (int direction in fittestDirections)
        {
            position = MoveToNextTile(position, direction);

            go = Instantiate(m_Tiles[4]);
            tilePosition = new Vector3(position.x - m_CamOffsets[0], -position.y + m_CamOffsets[1], 0.0f);
            go.transform.position = tilePosition;
            m_PathTiles.Add(go);
        }
    }
}
