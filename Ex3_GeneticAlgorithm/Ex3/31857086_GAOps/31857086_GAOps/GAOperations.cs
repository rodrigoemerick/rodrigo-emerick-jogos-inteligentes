﻿using System;
using System.Collections.Generic;

namespace _31857086_GA_Operations
{
    class GAOperations<T>
    {
        private Genome<T>[] m_Genomes;
        public Genome<T>[] Genomes
        {
            get => m_Genomes;
        }

        int m_PopulationSize = 100;
        int m_ChromosomeLength = 50;

        Random m_Random;

        public GAOperations(int populationSize, int chromosomeLength)
        {
            m_Random = new Random((int)DateTime.UtcNow.Ticks);

            m_PopulationSize = populationSize;
            m_ChromosomeLength = chromosomeLength;

            CreateInitialPopulation();
        }

        private void CreateInitialPopulation()
        {
            m_Genomes = new Genome<T>[m_PopulationSize];

            for (int i = 0; i < m_PopulationSize; ++i)
            {
                m_Genomes[i] = new Genome<T>(m_ChromosomeLength);
            }
        }

        //Selection
        public Genome<T>[] ElitismSelection(int howManyToNextGen)
        {
            List<Genome<T>> selectedGenomes = new List<Genome<T>>();
            int selectedGenome = 0;

            double bestFitness = 0.0f;

            while (howManyToNextGen > 0)
            {
                for (int i = 0; i < m_PopulationSize; ++i)
                {
                    if (m_Genomes[i].Fitness > bestFitness)
                    {
                        selectedGenome = i;
                        bestFitness = m_Genomes[i].Fitness;
                    }
                }
                if (!selectedGenomes.Contains(m_Genomes[selectedGenome]))
                {
                    selectedGenomes.Add(m_Genomes[selectedGenome]);
                    m_Genomes[selectedGenome].Fitness = 0.0f; //zerar a fitness do genoma escolhido para ele ser desconsiderado das próximas iterações
                    --howManyToNextGen;
                    bestFitness = 0.0f;
                }
            }

            return selectedGenomes.ToArray(); //retornar como array para facilitar a cópia no novo array da próxima geração
        }

        public Genome<T>[] SteadyStateSelection(int howManyToKill) //sorteando indivíduos para morte de maneira aleatória
        {
            List<Genome<T>> genomesToKill = new List<Genome<T>>();
            int selectedGenomeToKill = 0;

            while (howManyToKill > 0)
            {
                selectedGenomeToKill = m_Random.Next(m_Genomes.Length - 1);
                if (!genomesToKill.Contains(m_Genomes[selectedGenomeToKill]))
                {
                    genomesToKill.Add(m_Genomes[selectedGenomeToKill]);
                }
                --howManyToKill;
            }

            List<Genome<T>> genomesCopy = new List<Genome<T>>(m_Genomes);

            for (int i = 0; i < m_Genomes.Length; ++i)
            {
                genomesCopy[i] = m_Genomes[i];
            }

            for (int i = 0; i < genomesCopy.Count; ++i)
            {
                if (genomesToKill.Contains(genomesCopy[i]))
                {
                    genomesCopy.Remove(genomesCopy[i]);
                }
            }

            return genomesCopy.ToArray();
        }

        public Genome<T> RouletteWheelSelection()
        {
            double totalFitnessScore = 0.0f;

            for (int i = 0; i < m_Genomes.Length; ++i)
            {
                m_Genomes[i].Fitness = m_Random.NextDouble(); //nesse caso, está sorteando uma fitness para cada genoma para ser considerado depois no cálculo da roleta
                totalFitnessScore += m_Genomes[i].Fitness;
            }

            double fitnessSlice = m_Random.NextDouble() * totalFitnessScore;
            double fitnessTotal = 0.0f;
            int selectedGenome = 0;

            for (int i = 0; i < m_PopulationSize; ++i)
            {
                fitnessTotal += m_Genomes[i].Fitness;
                if (fitnessTotal > fitnessSlice)
                {
                    selectedGenome = i;
                    break;
                }
            }

            return m_Genomes[selectedGenome];
        }

        public Genome<T>[] SUSSelection()
        {
            double totalFitnessScore = 0.0f;

            for (int i = 0; i < m_Genomes.Length; ++i)
            {
                m_Genomes[i].Fitness = m_Random.NextDouble();
                totalFitnessScore += m_Genomes[i].Fitness;
            }

            double slice = totalFitnessScore / m_PopulationSize;
            double fitnessTotal = 0.0f;

            List<Genome<T>> selectedGenomes = new List<Genome<T>>();

            for (int i = 0; i < m_PopulationSize; ++i)
            {
                fitnessTotal += m_Genomes[i].Fitness;
                if (fitnessTotal > i + slice)
                {
                    selectedGenomes.Add(m_Genomes[i]);
                }
            }

            return selectedGenomes.ToArray();
        }

        public Genome<T> TournamentSelection(int howManySelected)
        {
            List<Genome<T>> selectedGenomes = new List<Genome<T>>();
            int selectedGenome = 0;
            int randomGenome = 0;

            double bestFitness = 0.0f;

            while (howManySelected > 0)
            {
                randomGenome = m_Random.Next(m_Genomes.Length - 1);
                if (!selectedGenomes.Contains(m_Genomes[randomGenome]))
                {
                    selectedGenomes.Add(m_Genomes[randomGenome]);
                    --howManySelected;
                }
            }

            for (int i = 0; i < selectedGenomes.Count; ++i)
            {
                if (m_Genomes[i].Fitness > bestFitness)
                {
                    bestFitness = selectedGenomes[i].Fitness;
                    selectedGenome = i;
                }
            }

            return selectedGenomes[selectedGenome];
        }

        //Crossover
        private void SinglePointCrossover(int[] parent1, int[] parent2, int[] child1, int[] child2)
        {
            int crossoverPoint = m_Random.Next(0, m_ChromosomeLength - 1);

            for (int i = 0; i < crossoverPoint; ++i)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }

            for (int i = crossoverPoint; i < parent1.Length; ++i)
            {
                child1[i] = parent2[i];
                child2[i] = parent1[i];
            }
        }

        private void DoublePointCrossover(int[] parent1, int[] parent2, int[] child1, int[] child2)
        {
            List<int> crossoverPoints = new List<int>();

            int crossoverPoint1 = 0, crossoverPoint2 = 0;

            while (crossoverPoints.Count < 2)
            {
                crossoverPoint1 = m_Random.Next(m_ChromosomeLength - 1);
                if (!crossoverPoints.Contains(crossoverPoint1))
                    crossoverPoints.Add(crossoverPoint1);
                crossoverPoint2 = m_Random.Next(m_ChromosomeLength - 1);
                if (!crossoverPoints.Contains(crossoverPoint2))
                    crossoverPoints.Add(crossoverPoint2);
            }

            int maxPoint = Math.Max(crossoverPoints[0], crossoverPoints[1]);
            int minPoint = Math.Min(crossoverPoints[0], crossoverPoints[1]);

            
            for (int i = 0; i < minPoint; ++i)
            {
                child1[i] = parent2[1];
                child2[i] = parent1[i];
            }

            for (int i = maxPoint + 1; i < parent1.Length; ++i)
            {
                child1[i] = parent2[1];
                child2[i] = parent1[i];
            }

            for (int i = minPoint; i <= maxPoint; ++i)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }
        }

        private void MultiPointCrossover(int[] parent1, int[] parent2, int[] child1, int[] child2, int howManyCrossoverPoints)
        {
            List<int> crossoverPoints = new List<int>();
            int newCrossoverPoint = 0;

            while (howManyCrossoverPoints > 0)
            {
                newCrossoverPoint = m_Random.Next(parent1.Length - 1);
                if (!crossoverPoints.Contains(newCrossoverPoint))
                {
                    crossoverPoints.Add(newCrossoverPoint);
                    --howManyCrossoverPoints;
                }
            }

            List<int> pointsToSwap = new List<int>();

            while (pointsToSwap.Count < 2)
            {
                int selectedCrossoverPoint = m_Random.Next(crossoverPoints.Count - 1);
                if (!pointsToSwap.Contains(selectedCrossoverPoint))
                {
                    pointsToSwap.Add(selectedCrossoverPoint);
                }
            }

            for (int i = 0; i < parent1.Length; ++i)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }

            int temp1 = child1[pointsToSwap[0]];
            child1[pointsToSwap[0]] = child1[pointsToSwap[1]];
            child1[pointsToSwap[1]] = child1[pointsToSwap[0]];

            int temp2 = child2[pointsToSwap[0]];
            child2[pointsToSwap[0]] = child2[pointsToSwap[1]];
            child2[pointsToSwap[1]] = child2[pointsToSwap[0]];
        }

        private void SingleArithmeticCrossover(double[] parent1, double[] parent2, double[] child1, double[] child2)
        {
            int selectedGenePos = m_Random.Next(0, m_ChromosomeLength - 1);

            double alpha = m_Random.NextDouble();

            for (int i = 0; i < parent1.Length; ++i)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }

            child1[selectedGenePos] = alpha * parent2[selectedGenePos] + (1.0f - alpha) * parent1[selectedGenePos];
            child2[selectedGenePos] = alpha * parent1[selectedGenePos] + (1.0f - alpha) * parent2[selectedGenePos];
        }

        private void SimpleArithmeticCrossover(double[] parent1, double[] parent2, double[] child1, double[] child2)
        {
            int selectedStartPos = m_Random.Next(m_ChromosomeLength - 1);

            double alpha = m_Random.NextDouble();

            for (int i = 0; i < parent1.Length; ++i)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }

            for (int i = selectedStartPos; i < m_PopulationSize; ++i)
            {
                child1[i] = alpha * parent2[i] + (1.0f - alpha) * parent1[i];
                child2[i] = alpha * parent1[i] + (1.0f - alpha) * parent2[i];
            }
        }

        private void WholeArithmeticCrossover(double[] parent1, double[] parent2, double[] child1, double[] child2)
        {
            double alpha = m_Random.NextDouble();

            for (int i = 0; i < m_PopulationSize; ++i)
            {
                child1[i] = alpha * parent1[i] + (1.0f - alpha) * parent2[i];
                child2[i] = alpha * parent2[i] + (1.0f - alpha) * parent1[i];
            }
        }

        private void PartiallyMappedCrossover(double[] parent1, double[] parent2, double[] child1, double[] child2)
        {
            List<int> crossoverPoints = new List<int>();

            int crossoverPoint1, crossoverPoint2;

            while (crossoverPoints.Count < 2)
            {
                crossoverPoint1 = m_Random.Next(m_ChromosomeLength - 1);
                if (!crossoverPoints.Contains(crossoverPoint1))
                    crossoverPoints.Add(crossoverPoint1);
                crossoverPoint2 = m_Random.Next(m_ChromosomeLength - 1);
                if (!crossoverPoints.Contains(crossoverPoint2))
                    crossoverPoints.Add(crossoverPoint2);
            }

            int maxPoint = Math.Max(crossoverPoints[0], crossoverPoints[1]);
            int minPoint = Math.Min(crossoverPoints[0], crossoverPoints[1]);

            Dictionary<int, double> p1MP2 = new Dictionary<int, double>(); //parent 1 mapping parent 2
            Dictionary<int, double> p2MP1 = new Dictionary<int, double>(); //parent 2 mapping parent 1

            for (int i = minPoint; i < maxPoint; ++i)
            {
                p1MP2.Add(i, parent1[i]);
                p2MP1.Add(i, parent2[i]);
            }

            for (int i = 0; i < parent1.Length; ++i)
            {
                child1[i] = parent1[i];
                child2[i] = parent2[i];
            }

            for (int i = 0; i < m_PopulationSize; ++i)
            {
                if (p1MP2.ContainsValue(child1[i]))
                {
                    child1[i] = p2MP1[i];
                }
                if (p2MP1.ContainsValue(child2[i]))
                {
                    child2[i] = p1MP2[i];
                }
            }
        }

        private void OrderBasedCrossover(int[] parent1, int[] parent2, int[] child1, int[] child2, int howManyToSelect)
        {
            List<int> selectedGenes = new List<int>();
            int tempHowManyToSelect = howManyToSelect;
            int selectedGenesCounter = 0;
            int newSelectedGene;

            //Pai1 impondo ordem do pai2 no filho1
            while (howManyToSelect > 0)
            {
                newSelectedGene = m_Random.Next(m_ChromosomeLength - 1);
                if (!selectedGenes.Contains(newSelectedGene))
                {
                    selectedGenes.Add(newSelectedGene);
                    --howManyToSelect;
                }
            }

            for (int i = 0; i < parent2.Length; ++i)
            {
                if (selectedGenes.Contains(i))
                {
                    child1[i] = parent2[selectedGenes[selectedGenesCounter]];
                    ++selectedGenesCounter;
                }
                else
                {
                    child1[i] = parent2[i];
                }
            }

            //Pai2 impondo ordem do pai2 no filho2
            howManyToSelect = tempHowManyToSelect;
            newSelectedGene = 0;
            selectedGenesCounter = 0;
            selectedGenes.Clear();

            while (howManyToSelect > 0)
            {
                newSelectedGene = m_Random.Next(m_ChromosomeLength - 1);
                if (!selectedGenes.Contains(newSelectedGene))
                {
                    selectedGenes.Add(newSelectedGene);
                    --howManyToSelect;
                }
            }

            for (int i = 0; i < parent1.Length; ++i)
            {
                if (selectedGenes.Contains(i))
                {
                    child2[i] = parent1[selectedGenes[selectedGenesCounter]];
                    ++selectedGenesCounter;
                }
                else
                {
                    child2[i] = parent1[i];
                }
            }
        }

        private void PositionBasedCrossover(double[] parent1, double[] parent2, double[] child1, double[] child2, int[] values, int howManyToSelect)
        {
            List<int> selected = new List<int>();
            int randomGen = 0;

            while (howManyToSelect > 0)
            {
                randomGen = m_Random.Next(values.Length - 1);
                if (!selected.Contains(values[randomGen]))
                {
                    selected.Add(values[randomGen]);
                    --howManyToSelect;
                }
            }

            for (int i = 0; i < parent1.Length; ++i)
            {
                if (selected.Contains(i))
                {
                    child1[i] = parent1[i];
                    child2[i] = parent2[i];
                }
                else
                {
                    child1[i] = parent2[i];
                    child2[i] = parent1[i];
                }
            }
        }

        //Mutation
        private void BitInversionMutation(bool[] genes, double mutationRate)
        {
            for (int currentGene = 0; currentGene < genes.Length; ++currentGene)
            {
                if (m_Random.NextDouble() < mutationRate)
                {
                    genes[currentGene] = !genes[currentGene];
                }
            }
        }

        private void ExchangeMutation(int[] values, double mutationRate)
        {
            int index1, index2, temp;
            List<int> selectedGenes = new List<int>();

            while (selectedGenes.Count < 2)
            {
                index1 = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(index1))
                    selectedGenes.Add(index1);
                index2 = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(index2))
                    selectedGenes.Add(index2);
            }

            if (m_Random.NextDouble() < mutationRate)
            {
                temp = values[selectedGenes[0]];
                values[selectedGenes[0]] = values[selectedGenes[1]];
                values[selectedGenes[1]] = temp;
            }
        }

        private void InsertionMutation(int[] values, double mutationRate)
        {
            int selectedGenPos = 0, selectedGenNewPos = 0, tempGen = 0;
            List<int> selectedPositions = new List<int>();

            while (selectedPositions.Count < 2)
            {
                selectedGenPos = m_Random.Next(values.Length - 1);
                if (!selectedPositions.Contains(selectedGenPos))
                    selectedPositions.Add(selectedGenPos);
                selectedGenNewPos = m_Random.Next(values.Length - 1);
                if (!selectedPositions.Contains(selectedGenNewPos))
                    selectedPositions.Add(selectedGenNewPos);
            }

            if (m_Random.NextDouble() < mutationRate)
            {
                if (selectedPositions[1] < selectedPositions[0])
                {
                    tempGen = values[selectedPositions[0]];
                    for (int i = selectedPositions[1]; i < selectedPositions[0]; ++i)
                    {
                        values[i + 1] = values[i];
                    }
                    values[selectedPositions[1]] = tempGen;
                }
                else if (selectedPositions[1] > selectedPositions[0])
                {
                    tempGen = values[selectedPositions[1]];
                    for (int i = selectedPositions[1]; i < selectedPositions[0]; --i)
                    {
                        values[i] = values[i - 1];
                    }
                    values[selectedPositions[0]] = tempGen;
                }
            }
        }

        private void InverseMutation(int[] values, double mutationRate)
        {
            int startPos = 0, endPos = 0;
            List<int> selectedGenes = new List<int>();

            while (selectedGenes.Count < 2)
            {
                startPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(startPos))
                    selectedGenes.Add(startPos);
                endPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(endPos))
                    selectedGenes.Add(endPos);
            }

            if (m_Random.NextDouble() < mutationRate)
            {
                if (selectedGenes[0] < selectedGenes[1])
                    Array.Reverse(values, selectedGenes[0], selectedGenes[1]);
                else if (selectedGenes[0] > selectedGenes[1])
                    Array.Reverse(values, selectedGenes[1], selectedGenes[0]);
            }
        }

        private void ScrambleMutation(int[] values, double mutationRate)
        {
            int startPos = 0, endPos = 0;
            List<int> selectedGenes = new List<int>();

            while (selectedGenes.Count < 2)
            {
                startPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(startPos))
                    selectedGenes.Add(startPos);
                endPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(endPos))
                    selectedGenes.Add(endPos);
            }

            if (m_Random.NextDouble() < mutationRate)
            {
                if (selectedGenes[1] > selectedGenes[0])
                {
                    int i = values[selectedGenes[1]];
                    int temp = 0;

                    while (i > values[selectedGenes[0]])
                    {
                        int j = m_Random.Next(--i);
                        temp = values[i];
                        values[i] = values[j];
                        values[j] = temp;
                    }
                }
                else if (selectedGenes[1] < selectedGenes[0])
                {
                    int i = values[selectedGenes[1]];
                    int temp = 0;

                    while (i < values[selectedGenes[0]])
                    {
                        int j = m_Random.Next(++i);
                        temp = values[i];
                        values[i] = values[j];
                        values[j] = temp;
                    }
                }
            }
        }

        private void DisplacementMutation(int[] values, double mutationRate)
        {
            List<int> valuesList = new List<int>(values);

            for (int i = 0; i < values.Length; ++i)
            {
                valuesList[i] = values[i];
            }

            int startPos = 0, endPos = 0, displacementIndex = 0;
            List<int> selectedGenes = new List<int>();

            while (selectedGenes.Count < 3)
            {
                startPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(startPos))
                    selectedGenes.Add(startPos);
                endPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(endPos))
                    selectedGenes.Add(endPos);
                displacementIndex = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(displacementIndex))
                    selectedGenes.Add(displacementIndex);
            }

            int maxPoint = Math.Max(selectedGenes[0], selectedGenes[1]);
            int minPoint = Math.Min(selectedGenes[0], selectedGenes[1]);

            if (m_Random.NextDouble() < mutationRate)
            {
                int range = valuesList[maxPoint - minPoint];
                List<int> tempList = new List<int>(range + 1);
                int tempListPos = 0;

                for (int i = minPoint; i < maxPoint; ++i)
                {
                    tempList[tempListPos] = values[i];
                    ++tempListPos;
                }

                valuesList.RemoveRange(valuesList[minPoint], range);
                valuesList.InsertRange(valuesList[selectedGenes[2]], tempList);

                valuesList.CopyTo(values);
            }
        }

        private void DisplacedInversionMutation(int[] values, double mutationRate)
        {
            int startPos = 0, endPos = 0, displacementIndex = 0;
            List<int> selectedGenes = new List<int>();

            while (selectedGenes.Count < 3)
            {
                startPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(startPos))
                    selectedGenes.Add(startPos);
                endPos = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(endPos))
                    selectedGenes.Add(endPos);
                displacementIndex = m_Random.Next(values.Length - 1);
                if (!selectedGenes.Contains(displacementIndex))
                    selectedGenes.Add(displacementIndex);
            }

            if (m_Random.NextDouble() < mutationRate)
            {
                //Inversion
                if (selectedGenes[0] < selectedGenes[1])
                    Array.Reverse(values, selectedGenes[0], selectedGenes[1]);
                else if (selectedGenes[0] > selectedGenes[1])
                    Array.Reverse(values, selectedGenes[1], selectedGenes[0]);

                //Displacement
                List<int> valuesList = new List<int>(values);

                for (int i = 0; i < values.Length; ++i)
                {
                    valuesList[i] = values[i];
                }

                int maxPoint = Math.Max(selectedGenes[0], selectedGenes[1]);
                int minPoint = Math.Min(selectedGenes[0], selectedGenes[1]);

                if (m_Random.NextDouble() < mutationRate)
                {
                    int range = valuesList[maxPoint - minPoint];
                    List<int> tempList = new List<int>(range + 1);
                    int tempListPos = 0;

                    for (int i = minPoint; i < maxPoint; ++i)
                    {
                        tempList[tempListPos] = values[i];
                        ++tempListPos;
                    }

                    valuesList.RemoveRange(valuesList[minPoint], range);
                    valuesList.InsertRange(valuesList[selectedGenes[2]], tempList);

                    valuesList.CopyTo(values);
                }
            }
        }
    }
}