﻿using System;
using System.Collections.Generic;

namespace _31857086_GA_Operations
{
    class Genome<T>
    {
        private T[] m_Genes;
        public T[] Genes
        {
            get => m_Genes;
            set => m_Genes = value;
        }

        private double m_Fitness;
        public double Fitness
        {
            get => m_Fitness;
            set => m_Fitness = value;
        }

        public Genome(int size)
        {
            m_Genes = new T[size];
            m_Fitness = 0.0f;
        }

    }
}