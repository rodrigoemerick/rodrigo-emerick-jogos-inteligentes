# Implementação de diversos métodos de seleção, crossover e mutação para Algoritmos Genéticos  

## Passo 1: Importando o projeto  

Para importar o projeto, clone ou faça o download da pasta deste repositório. Coloque os arquivos Genome.cs e GeneticAlgorithm.cs na pasta de scripts de seu projeto.  

## Passo 2: Entendendo as classes  

A classe Genome é uma classe genérica que assumirá a função de cada indivíduo da população de seu projeto, contendo duas propriedades: Fitness e Genes, ambas de escrita e leitura.  

A classe GeneticAlgorithm contém um método que inicializa a população inicial e a implementação de 5 métodos de seleção, 9 métodos de crossover e 7 métodos de mutação. Todos serão explicados a seguir.  

## Passo 3: Os métodos  
Todos os métodos são públicos, então podem ser acessados de qualquer classe desde que sejam referenciados.

### Métodos de seleção  
- ElitismSelection: recebe 1 parâmetro de quantidade de indivíduos que serão selecionados para se manterem na próxima geração de forma aleatória. Retorna um array com a quantidade de indivíduos fornecida no parâmetro que possuem a melhor fitness.
- SteadyStateSelection: recebe 1 parâmetro de quantidade de indivíduos para matar de forma aleatória. Retorna um array com os indivíduos da população que não foram selecionados para serem mortos.
- RouletteWheelSelection: seleciona um indivíduo para reprodução, é baseada na fitness, isto é, quanto maior a fitness do indivíduo, maior a chance dele ser selecionado.
- SUSSelection (Stochatic Universal Sampling): coloca uma quantidade de ponteiros igual ao tamanho da população, igualmente distribuídos pela roleta, retorna todos os indivíduos que estiverem na região dos ponteiros.
- TournamentSelection: receb 1 parâmetro que determina a quantidade de indivíduos a serem selecionados aleatoriamente. Retorna o indivíduo com melhor fitness dentre os selecionados.  

### Métodos de crossover  
Todos os métodos de crossover recebem no mínimo 4 parâmetros: pai1, pai2, filho1, filho2 em forma de vetores, pois o que deve ser fornecido a esses parâmetros são o vetor de Genes implementado na classe Genome.
- SinglePointCrossover: seleciona um ponto aleatório que divide o indivíduo, copiando tudo que vem antes do ponto do pai1 para o filho1 e do pai2 para o filho2 e tudo que vem depois do ponto vice-versa.
- DoublePointCrossover: seleciona dois pontos aleatórios no indivíduo, recombina os genes do pai1 para o filho1 e do pai2 para o filho2 em todas as posições que estiverem entre os dois pontos.
- MultiPointCrossover: recebe um quinto parâmetro que determina o número de pontos de corte desejados. Após cortar o indivíduo, são sorteados dois pontos, fazendo o mesmo processo do DoublePointCrossover.
- SingleArithmeticCrossover: sorteia um gene específico que é recombinado nos filhos baseado em uma equação aritmética.
- SimpleArithmeticCrossover: sorteia um gene específico no qual todos os genes a partir dele até o fim do vetor são recombinados baseados em uma equação aritmética.
- WholeArithmeticCrossover: todos os genes do vetor são recombinados baseados em uma equação aritmética.
- PMXCrossover: seleciona dois pontos aleatórios no vetor, mapeando os valores das posições nesse intervalo de um pai para o outro e usando os valores mapeados para copiar nos filhos.
- OBXCrossover: seleciona uma quantidade de pontos de forma aleatória que é fornecida como parâmetro, cria o filho1 baseado na ordem dos pontos selecionados impostas no pai2 e vice-versa.
- PBXCrossover: seleciona diversos genes aleatórios, cujas posições nos vetores dos pais são mantidas nos vetores dos filhos.

### Métodos de mutação  
Todos os métodos de mutação recebem dois parâmetros, um vetor de genes, da mesma forma como nos métodos de crossover,  e uma taxa de mutação em double, na qual a mutação só ocorrerá caso um double sorteado entre 0 e 1 seja menor que essa taxa.  
- BitInversionMutation: inverte todos os genes (bitS) dos filhos.
- ExchangeMutation: seleciona dois genes aleatoriamente e os troca de posição no vetor.
- InsetionMutation: seleciona um gene aleatório e uma nova posição aleatória para o inserir dentro do vetor.
- InverseMutation: seleciona dois genes aleatoriamente, no range dentro desses genes, todos são invertidos de posição.
- ScrambleMutation: seleciona dois genes aleatoriamente, no range dentro desses genes, todos são embaralhados.
- DisplacementMutation: seleciona dois genes aleatoriamente e sorteia uma nova posição. O range dos genes selecionados é deslocado para a nova posição.
- Displaced InversionMutation: é uma combinação do DisplacementMutation com o InverseMutation.