1. Como representar um caminho a ser percorrido por um NPC?
 

O caminho pode ser representado por valores de direção (cima, baixo, esquerda, direita) e quantidade de passos (1, 2, 3 passos por exemplo), que são combinados e geram as ações que instruem o caminho do NPC.

2. Como definir a população inicial (cromossomos/caminhos)?

Pode ser definida por um vetor de inteiros de forma randômica, cujas ações são ficar parado, andar para a esquerda, andar para a direita, andar para cima e andar para baixo, e os comandos, respectivamente, representados por 0, 1, 2, 3 e 4.

3. Como avaliar a aptidão de cada cromossomo?

Pode criar um parâmetro que avalia o quão próximo do objetivo aquele cromossomo chegou. Um exemplo dessa avaliação é ter chegado mais próximo do destino com o menor número de passos dados.

4. Como selecionar os pais para gerar novos seres (filhos)?

Selecionando os dois melhores cromossomos, isto é, os dois com a melhor aptidão.

5. Como fazer o cruzamento dos pais?

Pode-se selecionar os genes dos pais e sortear entre eles. Por exemplo, selecionar o primeiro alelo de cada cromossomo, sortear para escolher um deles e mandar para o filho, repetindo este processo até formar um novo filho.

6. Como um filho pode sofrer mutação?

Um filho pode sofrer mutação dando um valor de chance em porcentagem do mesmo poder sofrer a mutação. Assim, caso caia dentro dessa chance, a mutação ocorrerá.

7. Como criar uma nova população/geração?

A partir do cruzamento de dois indivíduos que tiveram a melhor aptidão na população atual, dando uma chance de seus filhos sofrerem uma mutação, para que uma nova população seja criada, mas não a partir de clones. 
